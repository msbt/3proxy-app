#!/bin/bash

set -eux

mkdir -p /run/nginx/log /run/nginx/lib

if [[ ! -f /app/data/3proxy.cfg ]]; then
    echo "=> Detected first run"

	#cp /app/code/cfg/3proxy.cfg.sample /app/data/3proxy.cfg

	# write default cfg entries
echo "
nserver 1.1.1.1
nserver 8.8.8.8
nserver 8.8.4.4
nscache 65536

timeouts 1 5 30 60 180 1800 15 60

# log /tmp/3proxy.log
# logformat "L%t%. L%t.%. %N.%p %E %U %C:%c %R:%r %O %I %h %T"

auth strong
allow proxy
external 0.0.0.0
internal 0.0.0.0
maxconn 20
users proxy:CL:ChangeThisImmediately
# http
socks -p10111
# socks5
proxy -p10112
flush
" > /app/data/3proxy.cfg

fi


# get/set ports
if [ -z ${SOCKS_PORT+x} ]; then SOCKS_PORT="10111"; else echo "SOCKS_PORT is set to '$SOCKS_PORT'"; fi
if [ -z ${PROXY_PORT+x} ]; then PROXY_PORT="10112"; else echo "PROXY_PORT is set to '$PROXY_PORT'"; fi

# update port in cfg
sed -i "s/proxy -p.*/proxy -p${PROXY_PORT}/" /app/data/3proxy.cfg
sed -i "s/socks -p.*/socks -p${SOCKS_PORT}/" /app/data/3proxy.cfg

chown -R www-data.www-data /app/data /run/


echo "Starting 3proxy!"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i 3proxy
