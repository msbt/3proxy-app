FROM cloudron/base:0.10.0
MAINTAINER Authors name <support@cloudron.io>

WORKDIR /app/code

ENV PROXYVERSION=0.8.12


RUN mkdir -p /app/code/web /run/nginx/lib /run/nginx/logs /app/data/ && \
    wget https://github.com/z3APA3A/3proxy/archive/${PROXYVERSION}.tar.gz -O -| tar -xz -C /app/code --strip-components=1 && \
    make -f Makefile.Linux && \
    ln -s /app/data/3proxy.cfg /app/code/cfg/3proxy.cfg && \
    rm -rf /var/lib/nginx && ln -s /run/nginx/lib /var/lib/nginx


# nginx stuff
RUN rm /etc/nginx/sites-enabled/* && \
    rm -rf /var/log/nginx && \
    ln -s /run/nginx/log /var/log/nginx && \
    chown -R www-data.www-data /app/data/ /run/ /app/code/


# copy index.html
COPY index.html /app/code/web

ADD start.sh /app/
ADD nginx.conf /etc/nginx/sites-enabled/


# Supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf


CMD [ "/app/start.sh" ]
