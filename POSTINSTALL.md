3proxy does not integrates with cloudron. The default account credentials are

* User: `proxy`
* Pass: `ChangeThisImmediately`

**Edit 3proxy.cfg to change the password and/or add additional users**

users user1:CL:password "user2:CR:$1$passwordwithspecialcharacters"
allow user1,user2
